# MOS Scanfile Inventarisatie

Todo:

1) De huidige setup maakt gebruik van single core threading. Dit zorgt voor een doorlooptijd van ongeveer 6 uur. Het zou daarom verstandiger zijn om het script iets aan te passen, zodat het bijvoorbeeld zestien maal kan worden opgestart. Hierdoor zouden de CPU-threads beter benut worden.

2A) Er zit momenteel geen blacklist in het script. Het zou verstandig zijn om er een toe te voegen, omdat er momenteel veel onrelevante entries in het scanbestand staan. 
2B) Er is geen minimum voor hoe groot een bestand moet zijn om meegenomen te worden in de inventarisatie. Een minimum van een paar MB zou een hoop vermeldingen weglaten.

3A) Geen ondersteuning voor weeknummers in entries.
3B) Standaard maandinvulling (1 of 12) wordt meegenomen in eindresultaat.

#!/usr/bin/python

import datetime
from datetime import timezone, datetime, timedelta
import gc
import signal
import sys
import threading
import time
import logging
import traceback
import re
import json
import os
import mmap
import xxhash
from prompt_toolkit import prompt
from math import floor, ceil



# Check problems blacklist
# Not going through all of the lines because of buffer
# Check date problems


# Static globals
noWorkers = 16
fileName = "dmscanfs.5sept2020"
# fileName = "Split_dmscanfs_5sept_aa"
noLines = 125881458
# noLines = 3301271
# noLines = 0
scanDir = ""
# noLines = 0


# Dynamic globals
linesProcessed = 0
interrupt = False

fileSize = 0
workLoad = []
fileNames = {}
workers = []

baseDirList = [
    "/data/meteo/forecasts/",
    "/data/meteo/models/",
    "/data/meteo/",
    "/data/obsmsg/",
    "/data/obsenvisat/",
    "/data/obsomi/",
    "/data/obsombe/",
    "/data/obsrs/",
    "/data/obsis/",
    "/data/obstrop/",
    "/data/climate/",
    "/data/climecearth/",
    "/data/climreg/",
    "/data/seismo/",
    "/data/infra/",
    "/data/admin/",
    "/data/users/",
    "/data/metapg/",
]

blacklist = {
    "paths": [
        "/data/users",
        "/data/infra",
        "/data/admin",
        "/data/metapg",
        "/data/meteo/forecasts/vis3d/w3dx_archive/backup2mos"
    ]
}

datePatterns = [
        {"regex": r"/(\d{4})/(\d{2})/(\d{2})/", "sub": "/%Y/%m/%d/", "values": 'Ymd'},
        {"regex": r"/(\d{4})/(\d{2})/", "sub": "/%Y/%m/", "values": 'Ym'},
        {"regex": r"/(\d{4})/", "sub": "/%Y/", "values": 'Y'},
        
        {"regex": r"(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})", "sub": "%Y%m%d_%H%M", "values": 'YmdHM'},
        {"regex": r"(\d{4})(\d{2})(\d{2})-(\d{2})", "sub": "%Y%m%d-%d", "values": 'Ymdd'},
        
        {"regex": r"(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})", "sub": "%Y%m%d%H%M", "values": 'YmdHM'},
        {"regex": r"(\d{4})(\d{2})(\d{2})", "sub": "%Y%m%d", "values": 'Ymd'},
        
        {"regex": r"(\d{4})(\d{2})", "sub": "%Y%m", "values": 'Ym'},
        
        {"regex": r"wk-(\d{2})", "sub": "wk-%W", "values": 'W'},
        {"regex": r"wk_(\d{2})", "sub": "wk_%W", "values": 'W'},
        
        {"regex": r"(\d{4})-(\d{4})", "sub": "%Y-%Y", "values": 'YY'},
        {"regex": r"(\d{4})_(\d{4})", "sub": "%Y_%Y", "values": 'YY'},
        
        {"regex": r"(\d{4})-(\d{2})", "sub": "%Y-%m", "values": 'Ym'},
        {"regex": r"(\d{4})_(\d{2})", "sub": "%Y_%m", "values": 'Ym'},
        
        {"regex": r"(\d{4})-(\d{1,})tot(\d{1,2})", "sub": "%Y-%mtot%m", "values": 'Ymm'},
        {"regex": r"(\d{4})", "sub": "%Y", "values": 'Y'}
    ]

skippedCtr = 0
skipped = []
datasets = {}
extensions = {}
programStarted = False
programStart = None
reporterInterval = 2
scanDir = ""


# Some UI stuff
def makeChoice(questionOne, questionTwo, choices):
    global interrupt 
    
    print(questionOne)
    
    for choice in choices:
        print(choice)
    
    validAnswer = False
    while not validAnswer and not interrupt:
        answer = getInt(questionTwo)
        if answer >= 0 and answer <= len(choices) - 1:
            validAnswer = True
    
    if interrupt:
        interrupt = False
        return
    
    return answer

def getInt(question):
    validAnswer = False
    answer = -1
    while not validAnswer:
        answer = input(question)
        try:
            answer = int(answer)
            validAnswer = True
        except ValueError:
            continue;
    return answer

def getYesOrNo(question):
    validAnswer = False
    answer = None
    while not validAnswer:
        answer = str(input(question)).lower()
        
        if answer == 'y' or answer == 'yes':
            answer = True
            validAnswer = True
        elif answer == 'n' or answer == 'no':
            answer = False
            validAnswer = True
        else:
            continue
    return answer

def getString(question, default=""):
    return str(prompt(question, default=default))


# Line parsing helper functions
def determinePaths(line):
    global baseDirList
    
    try:
        lineParts = line.split()
        fullPath = lineParts[18].lower()
        
        # If path corresponds to basedir slice out basedir, else stop
        found = False
        for baseDir in baseDirList:
            if baseDir in fullPath:
                smallPath = fullPath[len(baseDir):]
                found = True
        
        if not found:
            logging.critical(fullPath)
            signalWorkStop()
       
        smallPathSplit = smallPath.split('/')
        smallPathWOFN = '/'.join(smallPathSplit[:-1])
        smallPathWOFNSplit = smallPathSplit[:-1]
        
        return(fullPath, smallPath, smallPathWOFN, smallPathSplit, smallPathWOFNSplit, lineParts[:-1])
    except:
        return False

def isPathInBlacklist(path):
    global blacklist
    
    for blPath in blacklist["paths"]:
        if blPath in path:
            return True
    return False

def addSkipped(path):
    global skipped, skippedCtr
    
    skipped.append(path)
    skippedCtr += 1
    
    if skippedCtr == 10000:
        saveSkipped()
        skipped = []
        skippedCtr = 0

def determineNameAndKey(smallPathWOFNSplit):
    
    name = ' '.join(smallPathWOFNSplit)
    name = re.sub('[^0-9a-z]+', ' ', name)
    
    name = name.replace("archief", '')
    name = name.replace("archive", "")
    name = re.sub(r"\d", "", name)
    
    name = name.split()
    parts = []
    for part in name:
        if not part.isnumeric():
            if len(part) <= 3:
                parts.append(part.upper())
            else:
                parts.append(part.capitalize())
    
    name = ' '.join(parts)
    key = xxhash.xxh32(name).hexdigest()
    # key = hashlib.sha1(name.encode('utf-8')).hexdigest()[:10]
    
    return (name, key)

def addNewSubset(key, name):
    global datasets
    
    datasets[key] = {
        "name": name,
        "noFiles": 0,
        "totalSize": 0,
        "minSize": 0,
        "maxSize": 0,
        "minDate": "",
        "maxDate": "",
        "minDateUnix": None,
        "maxDateUnix": None,
        "paths": [],
        "accessQueue": []
    }

def freeDatasetLock(workerId, work):
    global datasets
    
    accessQueue = work["dataset"]["accessQueue"]
    if workerId == accessQueue[0]:
        accessQueue.pop(0)
    
    work["lock"] = ""
    work["dataset"] = None

def gainDatasetLock(workerId, work, key):
    global datasets, workLoad
    
    dataset = datasets[key]
    accessQueue = dataset["accessQueue"]
    if workerId not in accessQueue:
        accessQueue.append(workerId)
    
    while workerId != accessQueue[0]:
        time.sleep(1)
    
    work["lock"] = key
    work["dataset"] = dataset

def determineAndAddPathPattern(key, fullPath, smallPathWOFN):
    global datasets
    
    pattern = smallPathWOFN
    pattern = pattern.replace('+', "\+")
    pattern = pattern.replace('*', "\*")
    pattern = pattern.replace('?', "\?")
    pattern = pattern.replace('{', "\{")
    pattern = pattern.replace('}', "\}")
    
    pattern = re.sub(r"\d{1,}", "\\\d{1,}", pattern)
    pattern = "^{}/".format(pattern)
    
    found = False
    for patternSet in datasets[key]["paths"]:
        if patternSet["regex"] == pattern:
            found = True
            break
    
    if not found:
        datasets[key]["paths"].append({"regex": pattern, "path": fullPath})

def extractSizeInfo(lineParts):
    return int(lineParts[14])

def incrementDatasetTotal(d):
    d["noFiles"] += 1

def updateDatasetSize(d, size):
    d["totalSize"] += size
    
    if d["minSize"] == 0 or (d["minSize"] > 1024 and size < d["minSize"]):
        d["minSize"] = size
    
    if d["maxSize"] == 0 or (d["maxSize"] > 1024 and size > d["maxSize"]):
        d["maxSize"] = size

def updateDatasetDate(d, minDate, minDateUnix, maxDate, maxDateUnix):
    if d["minDateUnix"] == None or minDateUnix < d["minDateUnix"]:
        d["minDate"] = minDate
        d["minDateUnix"] = minDateUnix
    
    if d["maxDateUnix"] == None or maxDateUnix > d["maxDateUnix"]:
        d["maxDate"] = maxDate
        d["maxDateUnix"] = maxDateUnix

def mergeDatasets():
    global datasets, workLoad
    
    for work in workLoad:
        for key, w in work["data"].items():
            if key not in datasets:
                datasets[key] = w
            else:
                d = datasets[key]
                
                # Merge generic totals
                d["noFiles"] += w["noFiles"]
                d["totalSize"] += w["totalSize"]
                
                # Merge size counters
                if d["minSize"] > w["minSize"]:
                    d["minSize"] = w["minSize"]
                if d["maxSize"] < w["maxSize"]:
                    d["maxSize"] = w["maxSize"]
                
                # Merge dates
                if d["minDateUnix"] > w["minDateUnix"]:
                    d["minDateUnix"] = w["minDateUnix"]
                if d["maxDateUnix"] < w["maxDateUnix"]:
                    d["maxDateUnix"] = w["maxDateUnix"]
                
                # Merge paths
                for wPath in w["paths"]:
                    found = False
                    for dPath in d["paths"]:
                        if wPath["regex"] == dPath["regex"]:
                            found = True
                    if not found:
                        d["paths"].append(wPath)

def saveDatasets():
    global datasets, fileNames
    
    with open(fileNames["dataset"], "w+", encoding='utf-8') as file:
        json.dump(datasets, file, ensure_ascii=False, indent=4)

def saveExtensions():
    global extensions, fileNames
    
    with open(fileNames["extensions"], "w+") as file:
        file.write(str(extensions))

def saveException(msg):
    global fileNames
    
    with open(fileNames["exceptions"], "a+") as file:
        file.write(msg)

def saveSkipped():
    global skipped, fileNames
    
    with open(fileNames["skipped"], "a+") as file:
        file.write('\n'.join(skipped))

def saveFormatted():
    global datasets, fileNames
    
    # Collapse and sort data
    records = []
    for key in datasets:
        records.append(datasets[key])
    
    records = sorted(records, key=lambda k: k["name"])
    
    # Format and write data
    with open(fileNames["formatted"], "a+") as out:
        out.write("Naam,Aantal,Min Size (MiB),Max Size (MiB),Total (GiB), Average (MiB),Min date,Max date,Paths,\n")
        
        for r in records:
            minSize = "n/a" if r["minSize"] == None else round(int(r["minSize"]) / 1024 / 1024, 2)
            maxSize = "n/a" if r["maxSize"] == None else round(int(r["maxSize"]) / 1024 / 1024, 2)
            minDate = "n/a" if len(r["minDate"]) <= 1 else '-'.join(r["minDate"])
            maxDate = "n/a" if len(r["maxDate"]) <= 1 else '-'.join(r["maxDate"])
            totalSize = round(int(r["totalSize"]) / 1024 / 1024 / 1024, 2)
            average = round(int(r["totalSize"]) / int(r["noFiles"]) / 1024 / 1024, 2)
            
            paths = ""
            for patternSet in r["paths"]:
                path = patternSet["path"].replace("\r", "")
                path = path.replace("\n", "")
                paths += path + "; "
            
            out.write('"{}","{}","{}","{}","{}","{}","{}","{}","{}",\n'.format(r["name"], r["noFiles"], minSize, maxSize, totalSize, average, r["minDate"], r["maxDate"], paths))

def registerExtension(smallPathSplit):
    global extensions
    
    extension = os.path.splitext(smallPathSplit[-1])[1]
    if len(extension) < 9:
        if extension not in extensions:
            extensions[extension] = 1
        else:
            extensions[extension] += 1

def extractDateValues(fullPath):
    global datePatterns
    
    #extracted = {'Y': [], 'm': []}
    #tmp = {'Y': [], 'm': []}
    
    extracted = []
    
    tmpPath = fullPath
    for idx, pattern in enumerate(datePatterns):
        # On last item, filter out any remaining number so that single year item can be pinpointed
        if idx == len(datePatterns) - 1:
            tmpPath = re.sub(r"\d{5,}", "\\\d{5,}", tmpPath)
        
        matches = re.search(pattern["regex"], tmpPath)
        
        if matches == None:
            continue
        
        if len(pattern["values"]) == 1:
            matches = (matches.group(0),)
        else:
            matches = matches.groups()
        
        invalid = False
        dateTypeIdx = 0
        tmpExtract = {'Y': [], 'm': []}
        for valueStr in matches:
            value = int(re.sub("[^0-9]", "", valueStr))
            dateType = pattern["values"][dateTypeIdx]
            
            if (dateType == 'Y' and 1971 <= value <= 2022) \
                    or (dateType == 'm' and 1 <= value <= 12) \
                    or (dateType == 'd' and 1 <= value <= 31) \
                    or (dateType == 'H' and 0 <= value <= 24) \
                    or (dateType == 'M' and 0 <= value <= 59) \
                    or (dateType == 'W' and 0 <= value <= 52):
                dateTypeIdx += 1
                
                if dateType in "Ym":
                    tmpExtract[dateType].append(value)
            else:
                invalid = True
                break
        
        if not invalid:
            extracted.append(tmpExtract)
    
    return extracted
        
    """
    invalid = False
    dateTypeIdx = 0
    for valueStr in matches:
        value = int(re.sub("[^0-9]", "", valueStr))
        dateType = pattern["values"][dateTypeIdx]
        
        if (dateType == 'Y' and 1970 <= value <= 2030) or (dateType == 'm' and 1 <= value <= 12) or (dateType == 'd' and 1 <= value <= 31) or (dateType == 'H' and 0 <= value <= 24) or (dateType == 'M' and 0 <= value <= 59) or (dateType == 'W' and 0 <= value <= 52):
            dateTypeIdx += 1
            
            if dateType in "Ym" and value not in extracted[dateType]:
                tmp[dateType].append(value)
        else:
            invalid = True
    
    if not invalid:
        for dateType in "Ym":
            for value in tmp[dateType]:
                extracted[dateType].append(value)
        
        tmpPath = re.sub(pattern["regex"], pattern["sub"], tmpPath)
    
    return (tmpPath, extracted)
    """

def filterDateValues(extracted):
    # Extract date information and format date strings
    f = {
        'Y': {"min": 9999, "max": 0}, 
        'm': {"min": 12, "max": 1}
    }
    
    # Determine which values get a place in the final extract
    for extract in extracted:
        for year in extract['Y']:
            if year < f['Y']["min"]:
                f['Y']["min"] = year
            if year > f['Y']["max"]:
                f['Y']["max"] = year
        
        for month in extract['m']:
            for year in extract['Y']:
                if year == f['Y']["min"] and month < f['m']["min"]:
                    f['m']["min"] = month
                if year == f['Y']["max"] and month > f['m']["max"]:
                    f['m']["max"] = month
    
    # Fill empty spots if any remain
    if f['Y']["min"] == 9999 and f['Y']["max"] != 0:
        f['Y']["min"] = f['Y']["max"]
    elif f['Y']["min"] != 9999 and f['Y']["max"] == 0:
        f['Y']["max"] = f['Y']["min"]
    elif f['Y']["min"] == 9999 or f['Y']["max"] == 0:
        return False
    
    return f
    
    """
    noDate = False
    for dateType in "Ym":
        values = extracted[dateType]
        
        if len(values) == 0:
            if dateType == 'Y':
                noDate = True
                
            elif dateType == 'm':
                # Don't need to check if date value available because was already
                # (Lists are walked in order they are added)
                formatted['m']["min"] = 12
                formatted['m']["max"] = 1
        
        for value in values:
            if formatted[dateType]["min"] == None:
                formatted[dateType]["min"] = value
            if formatted[dateType]["max"] == None:
                formatted[dateType]["max"] = value
            
            if value < formatted[dateType]["min"]:
                formatted[dateType]["min"] = value
            elif value > formatted[dateType]["max"]:
                formatted[dateType]["max"] = value
    
    return (formatted, not noDate)
    """

def formatDateValues(filtered):
    f = filtered
    
    minDate = "{}-{}".format(f['Y']["min"], f['m']["min"])
    maxDate = "{}-{}".format(f['Y']["max"], f['m']["max"])
    
    minDateUnix = datetime.strptime("{}-{}".format(minDate, 27), "%Y-%m-%d")
    minDateUnix = time.mktime(minDateUnix.timetuple())
    
    maxDateUnix = datetime.strptime("{}-{}".format(maxDate, 1), "%Y-%m-%d")
    maxDateUnix = time.mktime(maxDateUnix.timetuple())
    
    return (minDate, minDateUnix, maxDate, maxDateUnix)

def extractDateInfo(fullPath):
    extracted = extractDateValues(fullPath)
    
    if len(extracted) == 0:
        return False
    
    filtered = filterDateValues(extracted)
    
    if filtered == False:
        return False
    
    return formatDateValues(filtered)
    
    """
    (pathPattern, extracted) = findRawDateValues(fullPath)
    if not pathPattern:
        return False
    
    (filtered, doesContainDates) = filterRawDateValues(extracted)
    
    print(str(filtered))
    exit()
    
    if doesContainDates:
        return formatRawDateValues(filtered)
    return False
    """



class StopWork(Exception): pass

# Line parsing main function
def parseLine(workerId, work, line):
    global workLoad, interrupt, datasets, linesProcessed, interrupt
    
    linesProcessed += 1
    
    """
    # TIMER BLOCK
    if not "timeSet" in globals():
        globals()["timeSet"] = True
        globals()["start"] = 0
        globals()["end"] = 0
        globals()["avg"] = 0
        globals()["total"] = 0
        globals()["donedataset"] = 0
    global timeSet, start, end, avg, total, done
    start = datetime.now()
    """
    
    if interrupt:
        raise StopWork
    
    try:
        # Split line into parts and parse paths
        result = determinePaths(line)
        
        if not result:
            return addSkipped(line)
        
        (fullPath, smallPath, smallPathWOFN, smallPathSplit, smallPathWOFNSplit, lineParts) = result
        
        # Determine the name and identifying key of the dataset
        (name, key) = determineNameAndKey(smallPathWOFNSplit)
        
        # Determine if still working on the same set
        if key != work["currSet"]:
            work["currSet"] = key
            
            # Check if any lock needs to be freed before checking blacklist
            if work["lock"] != "":
                # Free previous dataset lock
                freeDatasetLock(workerId, work)
            
            # Check if this key needs to be skipped
            work["keyValid"] = not isPathInBlacklist(fullPath)
            if not work["keyValid"]:
                return addSkipped(fullPath)
            
            # Add new subset to dataset if doesn't exist
            if key not in datasets:
                addNewSubset(key, name)
            
            # Gain access to dataset
            gainDatasetLock(workerId, work, key)
            
            # Determine path and add if it is yet unknown
            determineAndAddPathPattern(key, fullPath, smallPathWOFN)
        else:
            # Check if key is already on blacklist
            if work["currSet"] == key and not work["keyValid"]:
                return addSkipped(fullPath)
        
        # Extract filesize information
        size = extractSizeInfo(lineParts)
        
        # Extract date information
        dateValues = extractDateInfo(fullPath)
        
        # Update meta information about dataset
        incrementDatasetTotal(work["dataset"])
        updateDatasetSize(work["dataset"], size)
        
        if dateValues:
            (minDate, minDateUnix, maxDate, maxDateUnix) = dateValues
            updateDatasetDate(work["dataset"], minDate, minDateUnix, maxDate, maxDateUnix)
        
        # Add extension to list of known extensions
        # registerExtension(smallPathSplit)
        
        """
        # TIMER BLOCK
        end = datetime.now()
        done += 1
        diff = (end - start)
        total += diff.total_seconds() * 1000
        
        if done % 10000 == 0:
            avg = total / 10000
            total = 0
            done = 0
            print("{}: {}, {}".format(workerId, done, avg))
        """
        
        return True
    
    except Exception as e:
        handleException(e, "parseLine; WorkerId {}; line {};".format(workerId, line))
        return False

# Main worker function
def doWork(workerId):
    global interrupt
    
    try:
        # Set initial variables and start work
        work = workLoad[workerId]
        lines = []
        linesCtr = 0
        
        with open(work["inFileName"], 'r') as file:
            for line in file:
                lines.append(line)
                linesCtr += 1
                
                if linesCtr == 500:
                    linesCtr = 0
                    for line in lines:
                        parseLine(workerId, work, line)
                    lines = []
            if linesCtr != 0:
                for line in lines:
                    parseLine(workerId, work, line)
    except StopWork:
        pass
    except Exception as e:
        handleException(e, "doWork; WorkerId {}; line {};".format(workerId, line))
    
    if work["lock"] != "":
        freeDatasetLock(workerId, work)
    
    logging.info("(*) Worker {} done!".format(workerId))
    work["done"] = True



# Progress reporter
def reportProgress():
    global interrupt, linesProcessed, noLines, noWorkers, workLoad
    global programStarted, programStart, reporterInterval
    
    while not interrupt:
        if not programStarted:
            time.sleep(reporterInterval)
            continue
        
        # Calculate progress in percentages
        done = round(linesProcessed / noLines * 100, 2)
        percStr = "Processed {}%".format(done)
        
        # Calculate time left
        now = datetime.now()
        passed = (now - programStart).total_seconds()
        
        if done != 0:
            seconds = ((100 / done) * passed) - passed
            leftStr = "{} left".format(time.strftime("%H:%M:%S", time.gmtime(seconds)))
        else:
            leftStr = "??:??:?? left"
        
        # Format report string
        logging.info("(*) {}\t{}".format(percStr, leftStr))
        
        time.sleep(reporterInterval)

def handleException(e, msg):
    lines = traceback.format_exc()
    logging.critical("(-) A critical exception occured during program execution")
    
    saveException("{} {}\r\n\r\n".format(msg, lines))
    signalWorkStop()
    exit()

def countFileLinesAndSize(fileName, fileSize):
    logging.info("(*) Reading scanfile")
    
    fileSize = os.path.getsize(fileName)
    
    noLines = 0
    with open(fileName, "rb") as file:
        for line in file:
            noLines += 1
    
    logging.info("(*) {} lines in scanfile".format(noLines))
    
    return (fileSize, noLines)

def determineWorkLoads(noLines, noWorkers, workerIds):
    global scanDir
    
    logging.info("(*) Calculating worker load")
    
    noLinesPerWorker = int(noLines / noWorkers)
    remainingLines = noLines - ((noWorkers - 1) * noLinesPerWorker)
    
    for workerId in range(0, noWorkers):
        # inFileName = "./tmp-{}/scanfile-{}".format(shiftId, workerId)
        inFileName = "./tmp/scanfile-{}".format(workerId)
        outfileName = "{}/datasets-{}.csv".format(scanDir, workerId)
        
        if workerId in workerIds:
            workLoad.append({
                "id": workerId, 
                "noLines": noLinesPerWorker, 
                "inFileName": inFileName,
                "outFileName": outfileName,
                "done": False, 
                "error": False, 
                "lock": "",
                "currSet": "",
                "keyValid": None,
                "dataset": None
            })
    
    return workLoad

def createFilesAndFolders():
    global scanDir

    # Create scan directory where files will be placed
    scanDir = "./scanrun-{}".format(datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))
    os.mkdir(scanDir)
    
    # Determine filenames
    fileNames = {
        "log":          "{}/record.log".format(scanDir),
        "dataset":      "{}/dataset.json".format(scanDir),
        "exceptions":   "{}/exceptions.txt".format(scanDir),
        "extensions":   "{}/extensions.txt".format(scanDir),
        "skipped":      "{}/skipped.csv".format(scanDir),
        "formatted":    "{}/formatted.csv".format(scanDir)   
    }
    
    # Create files
    for key, fileName in fileNames.items():
        open(fileName, 'x')
    
    return fileNames

def initLogging(logFileName):
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(filename=logFileName, format=format, level=logging.INFO, datefmt="%H:%M:%S")
    logging.getLogger().addHandler(logging.StreamHandler())

def initAndStartReporter():
    reporter = threading.Thread(target=reportProgress, args=())
    reporter.start()

def launchWorkers(noWorkers, workerIds):
    for workerId in range(0, noWorkers):
        if workerId in workerIds:
            worker = threading.Thread(target=doWork, args=(workerId,))
            workers.append(worker)

# Init functions and start workers
def initResources(doCount, doSplit, doSkip, workerIds):
    global fileName, fileSize, noLines
    global noWorkers, workLoad
    global fileNames
    global workers
    
    
    # Catch stop program interrupt
    signal.signal(signal.SIGINT, signalHandler)
    
    # Create files and folders
    fileNames = createFilesAndFolders()
    
    # Init logging facilities
    initLogging(fileNames["log"])
    
    # Disable crazy garbage collection
    # gc.disable()
    
    # Log init of program
    logging.info("(*) Preparing to scan {}".format(fileName))
    logging.info("(*) Initialising required resources")
    
    
    # Determine number of lines in file
    if doCount:
        (fileSize, noLines) = countFileLinesAndSize(fileName, fileSize)
    
    # Determine load per worker
    # whichShift = getInt("Which shift is this? ")
    workLoad = determineWorkLoads(noLines, noWorkers, workerIds)
    
    # Split big file into smaller files
    if doSplit:
        logging.info("(*) Splitting scanfile into smaller pieces")
        
        if not os.path.exists("tmp"):
            os.makedirs("tmp")
        
        try:
            for workerId in range(0, noWorkers):
                if os.path.isfile("./tmp/scanfile-{}".format(workerId)):
                    if not doSkip:
                        logging.critical("(-) A split scanfile already exists")
                        return False
                    else:
                        logging.info("(*) Created scanfile {} of {}".format(workerId + 1, noWorkers))
                        continue
                
                with open(fileName, "r") as file:
                    skippedFirst = False
                    currWorkerId = 0
                    noLinesThreshold = workLoad[currWorkerId]["noLines"]
                    out = open(workLoad[currWorkerId]["inFileName"], "a+")
                    lineCtr = 0
                    
                    for line in file:
                        if not skippedFirst:
                            skippedFirst = True
                            continue
                        
                        if lineCtr == noLinesThreshold and currWorkerId != noWorkers - 1:
                            lineCtr = 0
                            out.close()
                            currWorkerId += 1
                            noLinesThreshold = workLoad[currWorkerId]["noLines"]
                            out = open(workLoad[currWorkerId]["inFileName"], "a+")
                            
                            logging.info("(*) Created scanfile {} of {}".format(currWorkerId, noWorkers))
                        
                        out.write(line)
                        lineCtr += 1
                    
                    out.close()
        
        except Exception as e:
            handleException(e, "init;")
    
    
    # These ones won't be changed and don't need to be passed on
    # --------------------------------------------------------------
    # fileName, fileSize, noLines, noWorkers, workers, 
    # programStarted, scanDir
    
    
    # These ones won't be changed but do need to be passed on
    # --------------------------------------------------------------
    # datasetFileName, exceptionsFileName, extensionsFileName,
    # skippedFileName, formattedFileName, baseDirList, blacklist,
    # datePatterns
    
    
    # These ones will be changed by everyone
    # --------------------------------------------------------------
    # linesProcessed, interrupt, workLoad, skipped, datasets
    # extensions
    
    
    # Collect all values and launch processes
    launchWorkers(noWorkers, workerIds)
    
    # Create and start progress reporter
    initAndStartReporter()
    
    return True

# Signal handler
def signalHandler(sig, frame):
    global interrupt
    
    interrupt = True

def signalWorkStop():
    global interrupt
    
    interrupt = True

# Main function
if __name__ == "__main__":
    try:
        
        if not initResources(False, False, True, list(range(0, noWorkers))):
            logging.critical("(-) Could not init resources!")
            exit()
        
        logging.info("(+) Finished initialisation, starting work")
        
        # Actually start workers
        programStarted = True
        programStart = datetime.now()
        for worker in workers:
            worker.start()
        
        # Wait for workers to stop, then stop reporter
        doWait = True
        while doWait:
            allDone = True
            for work in workLoad:
                if not work["done"]:
                    allDone = False
                    time.sleep(1)
            if allDone:
                doWait = False
        
        for worker in workers:
            worker.join()
            time.sleep(.1)
        
        signalWorkStop()
        
        logging.info("(*) Processed {} of {} lines".format(linesProcessed, noLines))
        logging.info("(*) Checking for errors")
        
        # Check for errors
        for work in workLoad:
            if work["error"]:
                logging.critical("(-) An error occurred while parsing the scanfile")
                exit()
        
        logging.info("(+) The workers have successfully finished their task")
        # signalWorkStop()
        
        # for workerId in range(0, noWorkers):
        #    with open("dataset-{}.json".format(workerId), 'w+', encoding='utf-8') as file:
        #        json.dump(workLoad[workerId]["data"], file, ensure_ascii=False, indent=4)
        
        # Save dataset to file
        # mergeDatasets()
        saveDatasets()
        saveExtensions()
        saveSkipped()
        saveFormatted()
        
        # Something something
        logging.info("(*) Program reached end of execution")
        
    except Exception as e:
        handleException(e, "main;")
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        